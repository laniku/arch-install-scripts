#!/usr/bin/env bash
setup() {
  ip link
  timedatectl set-ntp true
  sudo pacman -Sy cfdisk
  clear
  echo "Note that the only disk that is hardcoded to work so far is /dev/sda."
  sleep 5
  clear
  echo "CFdisk will launch in a moment. Please partition your disk. So far this is only hardcoded to have one boot partition and one root."
  sleep 8
  cfdisk
  clear
  echo "Note that the only disk that is hardcoded to work so far is /dev/sda."
  sleep 5
  mkfs.ext4 /dev/sda2
  mkfs.fat -F 32 /dev/sda1
  mkdir /mnt/boot
  mount /dev/sda2 /mnt
  mount /dev/sda1 /mnt/boot
}
main() {
  pacstrap /mnt base base-devel devtools linux linux-zen linux-firmware man-pages man-db texinfo
  genfstab -U /mnt >> /mnt/etc/fstab
  arch-chroot /mnt hwclock --systohc
  locale-gen
  echo 'LANG=en_US.UTF-8' >> /mnt/etc/locale.conf
  arch-chroot /mnt mkinitcpio -P linux
  arch-chroot /mnt mkinitcpio -P linux-zen
  arch-chroot /mnt passwd
  arch-chroot /mnt pacman -S grub
  arch-chroot /mnt grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
  arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
  clear
  echo "Installation is finished. Please reboot."
}

setup
main